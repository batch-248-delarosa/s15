console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:


let firstName = "Carlo";
let lastName = "Dela Rosa";
let age ="25";
let hobbies=["playing piano","watching youtube","cooking"];
let workAddress= {
	houseNumber: 170,
	street:"purok5",
	city:"Silang",
	state:"Cavite",

};
console.log("firstName:"+firstName);
console.log("lastName:"+lastName);
console.log("age:"+age);
console.log("hobbies:"+hobbies);
console.log("workAddress:"+workAddress.houseNumber+""+workAddress.street+""+workAddress.city+""+workAddress.state);

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let MyfullNameis = "Steve Rogers";
	console.log("MyfullNameis:"+MyfullNameis);
	//let firstName="Steve Rogers";

	//let age = "40";
	let Mycurrentageis="40";
	///console.log("Myc_rrent age is: " + currentAge);
	console.log("Mycurrentageis:"+Mycurrentageis);
	//let friends = ["Tony","Bruce",Thor,Natasha,"Clint""Nick"];
	let Myfriendsare=["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	//console.log("My Friends are: ")
	//console.log(friends);
	console.log=("Myfriendsare"+Myfriendsare);
	//let profile = {

		//username "captain_america"
		//fullName: "Steve Rogers';
		//age: 40,
		//isActive: false,//
	let Myfullprofile ={
		userName:"captain america",
		fullName:"Steve Rogers",
		age:40,
		isActive:false
	};

console.log=("Myfullprofile:"+Myfullprofile.userName+""+Myfullprofile.fullName+""+Myfullprofile.age);
	//}
	//console.log("My Full Profile: ")
	//console.log(profile);

	//let fullName = "Bucky Barnes";
	//console.log("My bestfriend is: " + fullName);
let Mybestfriendis="Bucky";
console.log=("Mybestfriendis:"+Mybestfriendis);

	//const lastLocation = "Arctic Ocean";
	//lastLocation = "Atlantic Ocean";
	//console.log("I was found frozen in: " + lastLocation);//

const lastLocation="Arctic Ocean";
let Iwasfoundfrozenin="Atlantic Ocean";
console.log=("I wasfound Frozen in:"+Iwasfoundfrozenin);